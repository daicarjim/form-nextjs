import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { useState } from 'react'
import swal from 'sweetalert';



export default function Home() {

const [name, setName] = useState('')
const [email, setEmail] = useState('')
const [phone, setPhone] = useState('')
const [referido, setRef] = useState('')
const [message, setMessage] = useState('')
const [submitted, setSubmitted] = useState(false)

  const handleSubmit = (e) => {

     e.preventDefault()


    if (phone.length == 0 || name.length == 0 || email.length == 0 || referido.length == 0 || message.length == 0){
           swal("No Send!", "Please fill all items", "error")
           return false;
   }
     else{

     swal("Send Success!", "Thanks for contact us", "success")
    console.log('Sending')

    function reload() {
    document.location.reload();
    }
  setTimeout(reload, 5000);

   }

    let data = {
        name,
        email,
        phone,
        referido,
        message
    }
    fetch('/api/contact', {
      method: 'POST',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then((res) => {
        console.log('Response received')
        if (res.status === 200) {
            console.log('Response succeeded!')
            setSubmitted(true) 
            setName('')
            setEmail('')
            setPhone('')
            setRef('')
            setMessage('')
        }
    })
  }


  return (
<div className={styles.container}>
  <div className={styles.title}>
    CONTÁCTANOS
  </div>  
     <div className={styles.footerButton2}>
       <button className={styles.inputButton2}onClick={() => window.location = "http://www.rgyservicioscontables.ml"}>Regresar</button>
    </div>
 < form className={styles.main} >
  < formGroup className={styles.inputGroup} >
    < label htmlFor='name'>Nombre</label>
    < input type='text' onChange={(e)=>{setName(e.target.value)}} name='name' className={styles.inputField} />
  </formGroup>
  < formGroup className={styles.inputGroup} >
    < label htmlFor='email'>Correo Electrónico</label>
    < input type='email' onChange={(e)=>{setEmail(e.target.value)}} email='email' className={styles.inputField} />
  </formGroup>
  < formGroup className={styles.inputGroup} >
    < label htmlFor='phone'>Número Celular</label>
    < input type='phone' onChange={(e)=>{setPhone(e.target.value)}} phone='phone' className={styles.inputField} />
  </formGroup>
   < formGroup className={styles.inputGroup} >
    < label htmlFor='referido'>Referido</label>
    < select type='text' onChange={(e)=>{setRef(e.target.value)}} referido='referido' className={styles.inputField} >
        <option value=""></option>
        <option value="Miguel Velandia">Miguel Velandia</option>
        <option value="Pedro Ortiz">Pedro Ortiz</option>
        <option value="Rosa Ramirez">Rosa Ramirez</option>
        <option value="Otro">Otro</option>
    </select>
  </formGroup>
 < formGroup className={styles.inputGroup} >
    < label htmlFor='message'>Mensaje</label>
    < textarea type='text' onChange={(e)=>{setMessage(e.target.value)}} message='message' className={styles.inputMessage} />
  </formGroup>
     <div className={styles.footerButton1}>
      <button className={styles.inputButton1} onClick={(e)=>{handleSubmit(e)}} >Enviar</button>
    </div>

 </form >
</div>
   )

}
